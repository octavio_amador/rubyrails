class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	belongs_to :user
	validates_presence_of :title
	validates_length_of :body, :in => 10..400, :message => "la longitud debe ser de 10 a 400 caracteres"
end
